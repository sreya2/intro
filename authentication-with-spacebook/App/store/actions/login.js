import * as firebase from 'firebase';

export const loginSuccess = (user) => {
    return{
        type: 'LOGIN_SUCCESS',
        user: user
    };
};

export const loginFail = (error) => {
    return{
        type: 'LOGIN_FAIL',
        error: error
    }
};

export const userNotVerified = () => {
    return dispatch => {
        console.log('not verified');
        firebase.auth().signOut()
        .then(function() {
            alert("Error: user email has not been verified.");
            dispatch( loginFail({code: 'auth/emailNotVerified', message: 'The user email has not been verified.'}) );
        })
        .catch(function(error) {
            dispatch(loginFail(error));
        });
    };
};

export const userVerified = (user) => {
    console.log('verified');
    return dispatch => {
        dispatch(loginSuccess(user));
    };
};

export const checkUserVerification = (user) => {
    return dispatch => {
        console.log('verifying');
        if(user.emailVerified){
            dispatch(userVerified(user));
        }
        else{
            dispatch(userNotVerified());
        }
    };
};

export const login = (email, password) => {
    return dispatch => {
        console.log('entered login');
        firebase.auth().signInWithEmailAndPassword(email, password)
		.then((res) => {
            dispatch(checkUserVerification(res.user));
		})
		.catch((error) => {
            console.log(error);
            dispatch(loginFail(error));
		});		 
    };
};