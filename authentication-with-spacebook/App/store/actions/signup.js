import * as firebase from 'firebase';

export const signupSuccess = (user) => {
    return{
        type: 'SIGNUP_SUCCESS',
        user: user
    };
};

export const signupFail = (error) => {
    return{
        type: 'SIGNUP_FAIL',
        error: error
    }
};

export const emailVerification = (user) => {
    return dispatch => {
        user.sendEmailVerification()
        .then(() => {
            alert("Link to verify email sent.");
            console.log(user);
            dispatch(signupSuccess(user));
        }).catch((error)  => {
            alert("An error occurred: ", error.message);
            dispatch(signupFail(error));
        });        
    };
};

export const signup = (email, password) => {
    return dispatch => {
        firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(res => {
            dispatch(emailVerification(res.user));
        })
        .catch((error) => {
            dispatch(signupFail(error));
        });          
    };
};