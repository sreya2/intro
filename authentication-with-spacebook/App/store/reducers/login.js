const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    };
};

const initialState = {
    user: null,
    error: null
};

const loginSuccess = (state, action) => {
    return updateObject(state, {user : action.user});
};

const loginFail = (state, action) => {
    return updateObject(state, {error: action.error})
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case 'LOGIN_SUCCESS': 
            return loginSuccess(state, action);
        case 'LOGIN_FAIL': 
            return loginFail(state, action);
        default:
            return state;
    }
};

export default reducer;