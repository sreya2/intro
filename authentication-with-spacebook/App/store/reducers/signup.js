const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    };
};

const initialState = {
    user: null,
    error: null
};


const signupSuccess = (state, action) => {
    return updateObject(state, {user : action.user});
};

const signupFail = (state, action) => {
    return updateObject(state, {error: action.error})
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case 'SIGNUP_SUCCESS': 
            return signupSuccess(state, action);
        case 'SIGNUP_SUCCESS': 
            return signupFail(state,action);
        default:
            return state;
    }
};

export default reducer;