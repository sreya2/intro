import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import * as actions from '../../../store/actions';

import './NewTodo.css';

class NewPost extends Component {
    state = {
        title: '',
        info: '',
        priority: 'standard',
        submitted: false
    }

    componentDidMount(){
        console.log(this.props);
    }

    todoDataHandler = () => {
        const todoData = {
            title: this.state.title,
            info: this.state.info,
            priority: this.state.priority
        }
        this.props.onAddNewTodo(this.props.userId, todoData);
        this.setState({submitted: true});
    }

    render () {
        let redirect = null;
        if(this.state.submitted) {
            redirect= <Redirect to="/todos" />;
        }
        return (
            <div className="NewTodo">
                {redirect}
                <h1>Add a Todo</h1>
                <label>Title</label>
                <input type="text" value={this.state.title} onChange={(event) => this.setState({title: event.target.value})} />
                <label>Info</label>
                <textarea rows="4" value={this.state.info} onChange={(event) => this.setState({info: event.target.value})} />
                <label>Priority</label>
                <select value={this.state.priority} onChange={(event) => this.setState({priority: event.target.value})}>
                    <option value="standard">Standard</option>
                    <option value="high">High</option>
                    <option value="low">Low</option>
                </select>
                <button onClick={this.todoDataHandler}>Add Todo</button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        userId: state.userId
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onAddNewTodo: (userId, todo) => dispatch(actions.addTodo(userId, todo))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewPost);