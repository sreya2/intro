import React, {Component} from 'react';
import {Route, Link} from 'react-router-dom';
import {connect} from 'react-redux';
import Todo from '../../../components/Todo/Todo';
import './Todos.css';
import FullTodo from '../FullTodo/FullTodo';
import * as actions from '../../../store/actions';

class Todos extends Component{

    componentDidMount() {
        this.props.onLoad(this.props.userId);
    }

    render(){
        let todoObjs = <p style ={{textAlign: 'center'}}>Something went wrong.</p>;
        if(this.props.todos){
            todoObjs = this.props.todos.map( todo => {
                return (
                    <Link to={'/todos/' + todo.id} key={todo.id} >
                        <Todo 
                            key={todo.id}
                            title={todo.title}/>
                    </Link>
                );
            });
        }

        return(
            <div>
                <section className="Todos">
                    {todoObjs}
                </section>
                <Route path={this.props.match.url + '/:id'} component={FullTodo} />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        todos: state.todos,
        userId: state.userId
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onLoad: (userId) => dispatch(actions.getTodos(userId))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Todos);