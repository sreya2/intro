import React, { Component } from 'react';
import {Route, NavLink, Switch, Redirect} from 'react-router-dom';

import './Main.css';
import Todos from './Todos/Todos';
import NewTodo from './NewTodo/NewTodo';
import FullTodo from './FullTodo/FullTodo';
import Auth from './Auth/Auth';
import Logout from './Auth/Logout';

class Main extends Component {
    render () {
        return (
            <div className="Main">
                <header>
                    <nav>
                        <ul>
                            {/* switch between auth and signout */}
                            <li><NavLink 
                                to="/todos/"
                                exact>
                                Todos
                                </NavLink></li>
                            <li><NavLink to='/new-todo'
                                exact>
                                New Todo
                                </NavLink></li>
                            <li><NavLink to='/authentication'
                                exact>
                                Authentication
                                </NavLink></li>
                            <li><NavLink to='/logout'
                                exact>
                                Logout
                                </NavLink></li>
                        </ul>
                    </nav>
                </header>
                <Switch>
                    <Redirect from='/' exact to='/authentication' />
                    <Route path='/authentication' component={Auth} />
                    <Route path='/todos/:id' component ={FullTodo} />
                    <Route path="/todos" exact component={Todos} />
                    <Route path="/new-todo" component={NewTodo} />
                    <Route path='/logout' component={Logout} />
                    <Route render={() => <h1 style={{textAlign: "center"}}>Not found</h1>}/>
                </Switch>
            </div>
        );
    }
}

export default Main;