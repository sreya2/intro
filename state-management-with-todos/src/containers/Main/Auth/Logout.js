import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import * as actions from '../../../store/actions';

class Logout extends Component{
    render(){
        let redirect=<Redirect to="/authentication" />;
        if(this.props.userId !== null){
            redirect = <Redirect to="/logout" />;
            this.props.onLogout();
        }
        return(
            <div className="Logout">
                {redirect}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        userId: state.userId
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onLogout: () => dispatch(actions.userLogout())
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Logout);