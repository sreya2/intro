import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import './Auth.css';
import * as actions from '../../../store/actions';

class Auth extends Component{

    state = {
        email: '',
        password: '',
        isSignup: true
    }

    switchFormTypeHandler = () => {
        this.setState({...this.state, isSignup: !this.state.isSignup});
    }

    submitClickedHandler = () => {
        this.state.isSignup 
        ? this.props.onSignup(this.state.email, this.state.password) 
        : this.props.onLogin(this.state.email, this.state.password);
    }
    
    render(){
        let redirect = null;
        if(this.props.userId) {
            redirect= <Redirect to="/todos" />;
        }
        return(
            <div className="Auth">
                {redirect}
                <h1>{this.state.isSignup ? 'SIGNUP' : 'LOGIN'}</h1>
                <input type="email" placeholder="Enter e-mail here" value={this.state.email} onChange={(event) => this.setState({email: event.target.value})} /> <br/>
                <input type="password" placeholder="Enter password here" value={this.state.password} onChange={(event) => this.setState({password: event.target.value}) }/><br/>
                <button onClick={this.submitClickedHandler}>Submit</button> <br/>
                <button onClick={this.switchFormTypeHandler}>Switch to {this.state.isSignup ? 'Login' : 'Signup'}</button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        userId: state.userId
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onLogin: (email, password) => dispatch(actions.userLogin(email,password)),
        onSignup: (email, password) => dispatch(actions.userSignup(email,password))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth);