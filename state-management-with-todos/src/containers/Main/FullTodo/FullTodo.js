import React, { Component } from 'react';
import './FullTodo.css';
import * as actions from '../../../store/actions';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';

class FullPost extends Component {

    state ={
        deleted: false
    }

    componentDidMount(){
        if(this.props.match.params.id){
            if ( !this.props.loadedTodo || (this.props.loadedTodo && this.props.loadedTodo.id !== this.props.match.params.id) ) {
                this.props.onLoadFullTodo(this.props.userId, this.props.match.params.id);
            }
        }
    }

    deletePostHandler = () => {
        this.props.onDeleteTodo(this.props.userId, this.props.match.params.id);
        this.setState({deleted: true});
    }

    render () {
        let redirect = null;
        if(this.state.deleted) {
            redirect=<Redirect to="/todos" />;
        }
        let post = <p style={{textAlign:'center'}}>{redirect}Select a post!</p>
        if(this.props.match.params.id){
            post = <p style={{textAlign:'center'}}>{redirect}Loading...</p>;
        }
        if(this.props.loadedTodo){
            post = (
                <div className="FullPost">
                    {redirect}
                    <h1>{this.props.loadedTodo.title}</h1>
                    <p>{this.props.loadedTodo.priority} priority</p>
                    <p>{this.props.loadedTodo.info}</p>
                    <div className="Edit">
                        <button onClick={this.deletePostHandler} className="Delete">Delete</button>
                    </div>
                </div>
            );
        }
        return post;
    }
}

const mapStateToProps = (state) => {
    return {
        loadedTodo : state.selectedTodo,
        userId : state.userId
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onLoadFullTodo: (userId, todoId) => dispatch(actions.getFullTodo(userId, todoId)),
        onDeleteTodo: (userId, todoId) => dispatch(actions.deleteTodo(userId, todoId))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FullPost);