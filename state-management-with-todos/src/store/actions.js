import {db} from '../index';
import * as firebase from 'firebase';

const getTodosStart = () => {
    return{
        type: 'GET_TODOS_START'
    }
}

const getTodosSuccess = (todos) => {
    return{
        type: 'GET_TODOS_SUCCESS',
        todos: todos
    }
}

const getTodosFail = (error) => {
    return{
        type: 'GET_TODOS_FAIL',
        error: error
    }
}

export const getTodos = (userId) => {
    return dispatch => {
        dispatch(getTodosStart());
        var updatedTodos =[];
        db.collection("users/"+userId+"/todos").get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                var todoData = {
                    id: doc.id,
                    title: doc.get("title")
                };
                updatedTodos.push(todoData);
            });
            dispatch(getTodosSuccess(updatedTodos));
        })
        .catch(err => {
            console.log(err);
            getTodosFail(err);
        });
    };
};

//alternate way of handling async code execution
export const getTodos2 = (userId) => {
    return async (dispatch)  => {
        dispatch(getTodosStart());
        var updatedTodos =[];
        const querySnapshot = await db.collection("users/"+userId+"/todos")
            .get()
            .catch(err => {
                console.log(err);
                getTodosFail(err);
            });
        querySnapshot.forEach((doc) => {
            var todoData = {
                id: doc.id,
                title: doc.get("title")
            };
            updatedTodos.push(todoData);
        });
        dispatch(getTodosSuccess(updatedTodos));
    };
};

const getFullTodoSuccess= (todo) => {
    return{
        type: 'GET_FULL_TODO_SUCCESS',
        fullTodo: todo
    }
}

const getFullTodoFail = (error) => {
    return{
        type: 'GET_FULL_TODO_FAIL',
        error: error
    }
}

export const getFullTodo = (userId, todoId) => {
    return dispatch => {
        var fullTodo = {};
        db.collection('users/'+userId+'/todos').doc(todoId).get().then( doc => {
            fullTodo = {
                id: doc.id,
                title: doc.get('title'),
                info: doc.get('info'),
                priority: doc.get('priority')
            };
            console.log(fullTodo);
            dispatch(getFullTodoSuccess(fullTodo));
        })
        .catch(err => {
            console.log(err);
            dispatch(getFullTodoFail(err));
        });
    }
}

const addTodoSuccess = (todo) => {
    return{
        type: "ADD_TODO_SUCCESS",
        newTodo: todo
    }
}

const addTodoFail = (error) => {
    return{
        type: "ADD_TODO_FAIL",
        error: error
    }
}

export const addTodo = (userId, todo) => {
    return dispatch => {
        db.collection("users/"+userId+"/todos").add(todo)
        .then((res)  => {
            const newTodo = {...todo, id: res.key};
            dispatch(addTodoSuccess(newTodo));
        })
        .catch((error) => {
            console.log(error);
            dispatch(addTodoFail(error));
        });
    };
}

const deleteTodoSuccess = () => {
    return{
        type: "DELETE_TODO_SUCCESS"
    }
}

const deleteTodoFail = (error) => {
    return{
        type: "DELETE_TODO_FAIL",
        error: error
    }
}

export const deleteTodo = (userId, todoId) => {
    return dispatch => {
        db.collection("users/"+userId+"/todos").doc(todoId).delete()
        .then( () => {
            console.log("Todo successfully deleted!");
            dispatch(deleteTodoSuccess());
        })
        .catch((error) => {
            console.log(error);
            dispatch(deleteTodoFail(error));
        });
    }
}

const userLoginSuccess = (id) => {
    return{
        type: 'USER_LOGIN_SUCCESS',
        userId: id
    }
}

const userLoginFail = (error) => {
    return{
        type: 'USER_LOGIN_FAIL',
        error: error
    }
}

const userNotVerified = () => {
    return dispatch => {
        firebase.auth().signOut()
        .then(function() {
            alert("Error: user email has not been verified.");
            dispatch( userLoginFail({code: 'auth/emailNotVerified', message: 'The user email has not been verified.'}) );
        })
        .catch(function(error) {
            dispatch( userLoginFail(error));
        });
    };
};

const userVerified = (user) => {
    return dispatch => {
        dispatch(userLoginSuccess(user.uid));
    };
};

const checkUserVerification = (user) => {
    return dispatch => {
        if(user.emailVerified){
            dispatch(userVerified(user));
        }
        else{
            dispatch(userNotVerified());
        }
    };
};

export const userLogin = (email, password) => {
    return async dispatch => {
        const response = await firebase.auth().signInWithEmailAndPassword(email, password)
        .catch((error) => {
            console.log(error);
            dispatch(userLoginFail(error));
        });
        dispatch(checkUserVerification(response.user));
    }
}

const userSignupSuccess = () => {
    return{
        type: 'USER_SIGNUP_SUCCESS'
    }
}

const userSignupFail = (error) => {
    return{
        type: 'USER_SIGNUP_FAIL',
        error: error
    }
}

const emailVerification = (user) => {
    return dispatch => {
        user.sendEmailVerification()
        .then(() => {
            alert("Link to verify email sent.");
            dispatch(userSignupSuccess(user));
        }).catch((error)  => {
            alert("An error occurred: ", error.message);
            dispatch(userSignupFail(error));
        });        
    };
}

export const userSignup = (email, password) => {
    return dispatch => {
        firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(res => {
            dispatch(emailVerification(res.user));
        })
        .catch((error) => {
            console.log(error);
            dispatch(userSignupFail(error));
        });
    }
}

const userLogoutSuccess = () => {
    return{
        type: 'USER_LOGOUT_SUCCESS'
    }
}

const userLogoutFail = (error) => {
    return{
        type: 'USER_LOGOUT_FAIL',
        error: error
    } 
}

export const userLogout = () => {
    return dispatch => {
        firebase.auth().signOut()
        .then(function() {
           dispatch(userLogoutSuccess());
        })
        .catch(function(error) {
            console.log(error);
            dispatch(userLogoutFail(error));
        });
    };
}