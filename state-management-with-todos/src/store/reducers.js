const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    };
};

const initialState= {
    todos: [],
    selectedTodo: null,
    error: null,
    userId: null
}

const getTodosStart = (state, action) => {
    return updateObject(state, {todos: [], selectedTodo: null})
}

const getTodosSuccess = (state, action) => {
    return updateObject(state, {todos: action.todos, selectedTodo: null})
}

const getTodosFail = (state,action) => {
    return updateObject(state, {error: action.error, todos:[], selectedTodo: null})
}

const getFullTodoSuccess = (state, action) => {
    return updateObject(state, {selectedTodo: updateObject(state.selectedTodo, action.fullTodo)});
}

const getFullTodoFail = (state,action) => {
    return updateObject(state, {error: action.error, selectedTodo: null})
}

const addTodoSuccess = (state, action) => {
    return updateObject(state, {selectedTodo: null});
}

const addTodoFail = (state, action) => {
    return updateObject(state, {error: action.error})
}

const deleteTodoSuccess = (state, action) => {
    return updateObject(state, {selectedTodo: null})
}

const deleteTodoFail = (state, action) => {
    return updateObject(state, {error: action.error, selectedTodo: null})
}

const userSignupSuccess = (state, action) => {
    return updateObject(state, {todos: [], selectedTodo: null})
}

const userSignupFail = (state, action) => {
    return updateObject(state, {error: action.error, todos:[], selectedTodo: null})
}

const userLoginSuccess = (state, action) => {
    return updateObject(state, {userId: action.userId})
}

const userLoginFail = (state, action) => {
    return updateObject(state, {error: action.error, userId: null})
}

const userLogoutSuccess = (state, action) => {
    return updateObject(state, {userId: null})
}

const userLogoutFail = (state, action) => {
    return updateObject(state, {error: action.error})
}

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case 'GET_TODOS_START': 
            return getTodosStart(state, action);
        case 'GET_TODOS_SUCCESS': 
            return getTodosSuccess(state, action);
        case 'GET_TODOS_FAIL': 
            return getTodosFail(state, action);
        case 'GET_FULL_TODO_SUCCESS': 
            return getFullTodoSuccess(state, action);
        case 'GET_FULL_TODO_FAIL': 
            return getFullTodoFail(state, action);
        case 'ADD_TODO_SUCCESS':
            return addTodoSuccess(state,action);
        case 'ADD_TODO_FAIL':
            return addTodoFail(state,action);
        case 'DELETE_TODO_SUCCESS':
            return deleteTodoSuccess(state,action);
        case 'DELETE_TODO_FAIL':
            return deleteTodoFail(state,action);
        case 'USER_SIGNUP_SUCCESS':
            return userSignupSuccess(state, action);
        case 'USER_SIGNUP_FAIL':
            return userSignupFail(state, action);
        case 'USER_LOGIN_SUCCESS':
            return userLoginSuccess(state, action);
        case 'USER_LOGIN_FAIL':
            return userLoginFail(state, action);
        case 'USER_LOGOUT_SUCCESS':
            return userLogoutSuccess(state, action);
        case 'USER_LOGOUT_FAIL':
            return userLogoutFail(state, action);
        default:
            return state;
    }
};

export default reducer;